/* Server stuff */
var io = require('socket.io-client');
var socket = io.connect('http://192.168.0.202/pccontroller');

/* ScreenLight running stuff */
var run = require('child_process').spawn;

socket.on('connect', function() {
	console.log('Connected');
});

// Start Theater
socket.on('theater', function() {
    console.log('Starting theater...');
    run('D:/Tools/bin/theater.bat');
});

socket.on('killtheater', function() {
    console.log('Stopping theater...');
    run('D:/Tools/bin/killtheater.bat');
});
