#include <VirtualWire.h>
#include <FastLED.h>
#include <MCom_defines.h>
#include <MCom.h>
#include <MCom_Arduino_rs232.h>
#include <MCom_Arduino_rf433.h>

#define CMD_COLORS 0x06
#define CMD_COLOR 0x05

enum {RS232, RF433};

#define PIN_COLOR 7
#define LED       13

#define NUM_LEDS 50

CRGB color;
CRGB leds[NUM_LEDS];

void Blink(Byte n) {
  Byte i;
  for (i = 0; i < n; i++) {
    digitalWrite(LED, HIGH);
    delay(1);
    digitalWrite(LED, LOW);
    delay(1);
  } 
}

void setColors(unsigned char cmd, unsigned int len, void *data) {
  CRGB *clr = (CRGB *)data;
  int i;
  for (i = 0; i < NUM_LEDS; i++) {
    leds[i].r = clr[i].r;
    leds[i].g = clr[i].g;
    leds[i].b = clr[i].b;
  }
  FastLED.show();
}

void setColor(unsigned char cmd, unsigned int len, void *data) {
  CRGB clr = *((CRGB *)data);
  int i;
  for (i = 0; i < NUM_LEDS; i++) {
    leds[i].r = clr.r;
    leds[i].g = clr.g;
    leds[i].b = clr.b;
  } 
  FastLED.show();
}

void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  // Initialize MCom w/ RF communicator
  Serial.begin(256000);
  MCom_init(256, 2, 2, 1000000);
  MCom_Arduino_rs232_init(RS232, Serial);
  
  MCom_bind(CMD_COLORS, 150, setColors);
  MCom_bind(CMD_COLOR, 3, setColor);
  
  // Initialize leds
  FastLED.addLeds<NEOPIXEL, PIN_COLOR, GRB>(leds, NUM_LEDS);
   
  color.r = 255; color.g = 134; color.b = 26;
  setColor(0, 0, &color);
  
  Blink(1);
}

void loop() {
   MCom_get(RS232);
}

