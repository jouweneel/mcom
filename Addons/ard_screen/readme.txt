ard_screen
----------
Programs an Arduino to display ScreenLight colors (i.e. DIY ambilight firmware) OR a single color on the entire NeoPixel strip

Things to configure:
- enum {CMD_COLORS, CMD_COLOR} -> Defines MCom commands, may also be set with #define CMD_COLORS 0x..
- #define PIN_COLOR -> NeoPixel strip pin
- #define NUM_LEDS -> Number of NeoPixels on the strip
