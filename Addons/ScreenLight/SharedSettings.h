#ifndef _SHAREDSETTINGS_H_
#define _SHAREDSETTINGS_H_

// Shared modes
#define MODE_IDLE   1
#define MODE_COLOR  2
#define MODE_SCREEN 3

extern bool s_flipped;

extern unsigned char s_debug;
extern long s_baudrate;
extern unsigned char s_mode;

extern unsigned char s_xLights;
extern unsigned char s_xLightsGap;
extern unsigned char s_yLights;
extern unsigned int s_nrLights;
extern unsigned int s_nrLightsBytes;

extern unsigned int s_boxWidth;
extern unsigned int s_boxHeight;
extern unsigned int s_xOffset;
extern unsigned int s_yOffset;
extern unsigned int s_boxArea;

void setLights(unsigned int xLights, unsigned int yLights, unsigned int xLightsGap, bool flipped);
void setBox(unsigned int boxWidth, unsigned int boxHeight);

#endif
