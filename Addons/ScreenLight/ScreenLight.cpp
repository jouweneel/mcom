#include <thread>

#include <WS2tcpip.h>

#include "SharedSettings.h"
#include "Screen.h"

#define BUFSIZE 1024

using namespace std;

SOCKET udp;
struct sockaddr_in serverInfo;

Screen screen;
RGB *lights;
bool newFrame = false;
char buf[BUFSIZE];

int nrFrames = 0;

// Initialization
void init(char *host, int port) {
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	int len = sizeof(serverInfo);
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_port = htons(port);
	serverInfo.sin_addr.s_addr = inet_addr(host);

	udp = socket(AF_INET, SOCK_DGRAM, 0);

    lights = new RGB[s_nrLights];
    screen.Init();

	printf("connecting to %s:%i \n", host, port);


	printf("connected");
}

void captureThread(char *route) {
	while (1) {
        lights = screen.GetFrame();
		int len = sprintf(buf, "%s%c", route, 0x00);
		memcpy(&(buf[len]), lights, s_nrLightsBytes);

		sendto(udp, buf, BUFSIZE, 0, (sockaddr*)& serverInfo, len + s_nrLightsBytes);
		nrFrames++;
        this_thread::yield();
    }
}

int main(int argc, char* argv[])
{
    int done = 0;

    if (argc <= 7) {
        printf("Usage: \"ScreenLight host user pass channel x y g f\"\n\thost: Target ip (192.168.0.2)\nport: UDP port (3333)\nroute: route to publish to \n\tx: Horizontal lights\n\ty: Vertical lights\n\tg: Bottom gap\n\tf: Flipped (0=false, 1=true)\n");
        return -1;
    }

	char *host = argv[1];
	int port = atoi(argv[2]);
	char *route = argv[3];

    unsigned int xLights = (int)atoi(argv[4]);
    unsigned int yLights = (int)atoi(argv[5]);
    unsigned int xLightsGap = (int)atoi(argv[6]);
    bool flipped = (bool)(atoi(argv[7]));

	setLights(xLights, yLights, xLightsGap, flipped);

	init(host, port);

    thread capture(captureThread, route);

    // Keep main program alive
    while (!done) {
        Sleep(10000);
        printf("fps: %d\n", nrFrames / 10);
        nrFrames = 0;
    }

    capture.join();

    return 0;
}
