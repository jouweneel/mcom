#include "SharedSettings.h"

unsigned char s_debug = 0;
long s_baudrate = 256000;

bool s_flipped = false;

unsigned char s_xLights = 0;
unsigned char s_xLightsGap = 0;
unsigned char s_yLights = 0;
unsigned int s_nrLights = 2*s_xLights + 2*s_yLights - s_xLightsGap;
unsigned int s_nrLightsBytes = s_nrLights * 4;

unsigned int s_boxWidth = 64;
unsigned int s_boxHeight = 64;
unsigned int s_xOffset = 0;
unsigned int s_yOffset = 0;
unsigned int s_boxArea = s_boxWidth * s_boxHeight;

void setLights(unsigned int xLights, unsigned int yLights, unsigned int xLightsGap, bool flipped) {
    s_xLights = xLights;
    s_yLights = yLights;
	s_xLightsGap = xLightsGap;
	s_flipped = flipped;
    s_nrLights = 2 * s_xLights + 2 * s_yLights - s_xLightsGap;
    s_nrLightsBytes = 4 * s_nrLights;
    return;
}

void setBox(unsigned int boxWidth, unsigned int boxHeight) {
    s_boxWidth = boxWidth;
    s_boxHeight = boxHeight;
    s_boxArea = boxWidth*boxHeight;
    return;
}
