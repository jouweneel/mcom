#include "Screen.h"

using namespace std;

Screen::Screen() {
    // Screen dimensions
    xSize = GetSystemMetrics(SM_CXSCREEN);
    ySize = GetSystemMetrics(SM_CYSCREEN);
    pixels = xSize * ySize;
    // Screen capture handles
    hdcSrc = GetDC(0);
    hdcMem = CreateCompatibleDC(hdcSrc);
    // Screen capture storage
    bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth = xSize;
    bmi.bmiHeader.biHeight = ySize;
    bmi.bmiHeader.biPlanes = 1;
    bmi.bmiHeader.biBitCount = 32;
    bmi.bmiHeader.biCompression = BI_RGB;
    // Local screen/color message storage
    Pixels = new RGBQUAD[pixels];
}

Screen::~Screen() {
    ReleaseDC(0, hdcSrc);
    DeleteDC(hdcMem);
}

// Initialize
void Screen::Init() {
    // Initialize arrays
    Lights = new RGB[s_nrLights];
    startsX = new unsigned int[s_nrLights];
    startsY = new unsigned int[s_nrLights];

    xTicks = new unsigned int[s_xLights];
    yTicks = new unsigned int[s_yLights];

    c_offset = 0;
    bar_bottom = 0;
    bar_right = 0;
    bar_top = 0;
    bar_left = 0;
    for (unsigned int i = 0; i < s_nrLights; i++) {
        Lights[i] = { 0, 0, 0 };
        startsX[i] = 0;
        startsY[i] = 0;
    }

    // Generate X and Y ticks
    unsigned int xRange = xSize - 2 * s_xOffset;
    unsigned int yRange = ySize - 2 * s_yOffset;

    for (int i = 0; i < s_xLights; i++) {
        xTicks[i] = s_xOffset + (unsigned int)round((float)i*((float)xRange / (float)(s_xLights - 1)));
    }
    for (int i = 0; i < s_yLights; i++) {
        yTicks[i] = s_yOffset + (unsigned int)round((float)i*((float)yRange / (float)(s_yLights - 1)));
    }

    // Generate starting X's and Y's
    unsigned int nrLightsBottom = (s_xLights - s_xLightsGap) >> 1;
    int startX = 0;
    int startY = 0;
    unsigned int index = 0;

    for (unsigned int i = 0; i < (nrLightsBottom); i++) {
        // Bottom left
        startX = (int)xTicks[nrLightsBottom - 1 - i] - (int)(s_boxWidth >> 1);
        if (startX < 0) startX = 0;

        index = (nrLightsBottom - 1 - i);
        startsX[i] = (unsigned int)startX;
        startsY[i] = c_offset;

        // Bottom right
        startX = (int)xTicks[s_xLights - 1 - i] - (s_boxWidth >> 1);
        if ((startX + s_boxWidth) >= xSize) startX = xSize - s_boxWidth - 1;

        index = i + nrLightsBottom + 2 * s_yLights + s_xLights;
        startsX[index] = (unsigned int)startX;
        startsY[index] = c_offset;
    }

    for (unsigned i = 0; i < s_yLights; i++) {
        // Left lights
        startY = (int)yTicks[i] - (int)(s_boxWidth >> 1);
        if (startY < 0) startY = 0;
        if ((startY + s_boxWidth) >= ySize) startY = ySize - c_offset - s_boxWidth - 1;

        index = i + nrLightsBottom;
        startsX[index] = c_offset;
        startsY[index] = (unsigned int)startY;

        // Right lights
        index = (nrLightsBottom + 2*s_yLights + s_xLights - i - 1);
        startsX[index] = xSize - 1 - c_offset - s_boxWidth;
        startsY[index] = (unsigned int)startY;
    }
    
    for (unsigned i = 0; i < s_xLights; i++) {
        // Top lights
        startX = (int)xTicks[i] - (int)(s_boxWidth >> 1);
        if (startX < 0) startX = 0;
        if ((startX + s_boxWidth) >= xSize) startX = xSize - s_boxWidth - 1;

        index = i + nrLightsBottom + s_yLights;
        startsX[index] = startX;
        startsY[index] = ySize - c_offset - s_boxHeight - 1;
    }

    bar_xStep = (xSize >> 2);
    bar_yStep = (ySize >> 2);
    /*
    for (unsigned int i = 0; i < s_nrLights; i++) {
        printf("Box %d:\t%d,\t%d\n", i, startsX[i], startsY[i]);
    }
    */
    return;
}

// Calculates AmbiLight colors for a single frame
RGB *Screen::GetFrame(void) {
    Capture();
    GetBlackBars();
    GetColors();

    return Lights;
}

// Captures a screenshot and stores it in Pixels
void Screen::Capture(void) {
    HBITMAP hBitmap = CreateCompatibleBitmap(hdcSrc, xSize, ySize);
    SelectObject(hdcMem, hBitmap);
    BitBlt(hdcMem, 0, 0, xSize, ySize, hdcSrc, 0, 0, SRCCOPY);
    GetDIBits(hdcSrc, hBitmap, 0, ySize, Pixels, &bmi, DIB_RGB_COLORS);
    DeleteObject(hBitmap);
    return;
}

void Screen::GetBlackBars() {
    bar_bottom = 0;
    bar_right = 0;
    bar_top = 0;
    bar_left = 0;
    unsigned char nul1 = 0x00;
    unsigned char nul2 = 0x00;

    // Bottom and top offsets
    for (unsigned int _y = 0; _y < 250; _y+=10) {
        for (unsigned int x = 0; x < xSize; x += bar_xStep) {
            unsigned int yBottom = _y*xSize;
            unsigned int yTop = (ySize - 1 - _y)*xSize;
            nul1 |= Pixels[x + yBottom].rgbRed;
            nul1 |= Pixels[x + yBottom].rgbGreen;
            nul1 |= Pixels[x + yBottom].rgbBlue;
            nul2 |= Pixels[x + yTop].rgbRed;
            nul2 |= Pixels[x + yTop].rgbGreen;
            nul2 |= Pixels[x + yTop].rgbBlue;
        }
        if (nul1 != 0x00 && nul2 != 0x00) {
            break;
        }
        if (nul1 == 0x00) {
            bar_bottom += 10;
        }
        if (nul2 == 0x00) {
            bar_top += 10;
        }
    }
    // Left and right offsets
    nul1 = 0x00;
    nul2 = 0x00;
    for (unsigned int x = 0; x < 250; x += 10) {
        for (unsigned int _y = 0; _y < ySize; _y += bar_yStep) {
            unsigned int y = _y*xSize;
            unsigned int xRight = xSize - 1 - x;
            nul1 |= Pixels[x + y].rgbRed;
            nul1 |= Pixels[x + y].rgbGreen;
            nul1 |= Pixels[x + y].rgbBlue;
            nul2 |= Pixels[xRight + y].rgbRed;
            nul2 |= Pixels[xRight + y].rgbGreen;
            nul2 |= Pixels[xRight + y].rgbBlue;
        }
        if (nul1 != 0x00 && nul2 != 0x00) {
            break;
        }
        if (nul1 == 0x00) {
            bar_left += 10;
        }
        if (nul2 == 0x00) {
            bar_right += 10;
        }
    }
    //printf("Bars: %d, %d, %d, %d\n", bar_bottom, bar_right, bar_top, bar_left);
    // Bottom border
    return;
}

// Calculates colors for each border section
void Screen::GetColors(void) {
    unsigned int width = 0;
    unsigned int height = 0;
    unsigned int startX = 0;
    unsigned int startY = 0;

    unsigned int nrBottomLights = (s_xLights >> 1) - s_xLightsGap;

    for (unsigned int i = 0; i < s_nrLights; i++) {
        // Bottom
        if (i < nrBottomLights || i >= (nrBottomLights + s_yLights + s_xLights + s_yLights)) {
            width = s_boxWidth;
            height = s_boxHeight;
            startX = startsX[i];
            startY = startsY[i] + bar_bottom;
        }
        // Left
        else if (i >= nrBottomLights && i < (nrBottomLights + s_yLights)) {
            width = s_boxHeight;
            height = s_boxWidth;
            startX = startsX[i] + bar_left;
            startY = startsY[i];
        }
        // Top
        else if (i >= (nrBottomLights + s_yLights) && i < (nrBottomLights + s_yLights + s_xLights)) {
            width = s_boxWidth;
            height = s_boxHeight;
            startX = startsX[i];
            startY = startsY[i] - bar_top;
        }
        // Right
        else if (i >= (nrBottomLights + s_yLights + s_xLights) && i < (nrBottomLights + s_yLights + s_xLights + s_yLights)) {
            width = s_boxHeight;
            height = s_boxWidth;
            startX = startsX[i] - bar_right;
            startY = startsY[i];
        }
		if (s_flipped) {
			Lights[s_nrLights - 1 - i] = CalculateColor(startX, startY, width, height);
		}
		else {
			Lights[i] = CalculateColor(startX, startY, width, height);
		}
    }

    return;
}

RGB Screen::CalculateColor(unsigned int startx, int unsigned starty, unsigned int boxWidth, unsigned int boxHeight) {
    int r = 0;
    int g = 0;
    int b = 0;

    for (unsigned int y = starty; y < (starty + boxHeight); y = y + 1) {
        for (unsigned int x = startx, size = startx + boxWidth; x < size; x = x + 1) {
            unsigned int yPix = y*xSize;
            r += Pixels[x + yPix].rgbRed;
            g += Pixels[x + yPix].rgbGreen;
            b += Pixels[x + yPix].rgbBlue;
        }
    }

    RGB color;
    color.b = (r >> 12) & 0xFF;
    color.g = (g >> 12) & 0xFF;
    color.r = (b >> 12) & 0xFF;
	color.a = 0xFF;
    //cout << "Calc r: " << (int)color.r << " g: " << (int)color.g << " b: " << (int)color.b << "\n";

    return color;
}
