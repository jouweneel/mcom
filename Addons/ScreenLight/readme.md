ScreenLight - DIY ambilight for Arduino with MCom
-------------------------------------------------
This is the beating heart of the home made ambilight. It captures desktop frames as fast as possible, sending them over RS232 to an Arduino with the MCom protocol.
It should compile right out of the box on Visual Studio 2013+. For older versions you should check the project properties and change the C++ runtime.

Things to configure (in SharedSettings.cpp)

- s_baudRate -> The RS232 baud rate. Should be matched with the Serial rate of your Arduino
- s_flipped, s_xLights, s_xLightsGap, s_yLights -> Defines the amount of NeoPixels that you want to address, and the starting point

Defined as follows ('0' represents a NeoPixel)

		  s_xLights
    <------------------->
	0 - 0 - 0 - 0 - 0 - 0
	|					| 
	0					0 ^
	|					| | s_yLights
	0	s_xLightsGap	0 v
	|		<--->		|
	0 - 0 - - - - - 0 - 0 
		|			|
		false		true	<- s_flipped depending on where the strip is connected
		
- s_xOffset, s_yOffset -> Constant offsets in the X and Y directions

Don't change s_boxWidth and s_boxHeight unless you know what you're doing - this involves changing the right shift division in screen.cpp!
