#ifndef _SCREEN_H_
#define _SCREEN_H_

#include <Windows.h>
#include <math.h>
#include "SharedSettings.h"

typedef struct RGB {
    unsigned char r;
    unsigned char g;
    unsigned char b;
	unsigned char a;
} RGB;

typedef struct ColorCount {
    unsigned int color;
    unsigned int count;
    unsigned int position;
} ColorCount;

class Screen{
private:
    // Screen dimensions
    unsigned int xSize;
    unsigned int ySize;
    unsigned int pixels;

    // Starting positions
    unsigned int *xTicks;
    unsigned int *yTicks;

    unsigned int *startsX;
    unsigned int *startsY;
    // Offsets
    unsigned int c_offset;
    unsigned int bar_xStep;
    unsigned int bar_yStep;
    unsigned int bar_bottom;
    unsigned int bar_right;
    unsigned int bar_top;
    unsigned int bar_left;
    // Screen capture handles
    HDC hdcSrc;
    HDC hdcMem;
    // Screen capture storage
    BITMAPINFO bmi;
    // Local screen/color message storage
    RGBQUAD* Pixels;
    RGB* Lights;
    
    // Private functions
    void Capture(void);
    void GetBlackBars(void);
    void GetColors(void);
    //RGB GetColor(int nr);
    RGB CalculateColor(unsigned int startx, int unsigned starty, unsigned int boxWidth, unsigned int boxHeight);
public:
    Screen();
    ~Screen();

    void Init(void);
    RGB *GetFrame(void);

    //void SetBarDetection(bool);
    //void SetOffset(unsigned int);
};

#endif
