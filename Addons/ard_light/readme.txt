ard_light
---------
Programs an Arduino to read incoming data via an RF433 radio receiver. 

Things to configure:
- #define CMD_COLOR -> Color command for MCom
- #define DATA      -> RF433 data pin
- #define COLOR     -> NeoPixel strip pin
- #define NUM_LEDS  -> Number of NeoPixels on the strip