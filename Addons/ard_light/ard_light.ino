#include <VirtualWire.h>
#include <MCom_defines.h>
#include <MCom.h>
#include <MCom_Arduino_rf433.h>
#include <FastLED.h>

#define CMD_COLOR 0x01
enum {RF433};

#define DATA 10
#define COLOR 4
#define LED 13
#define NUM_LEDS 40

CRGB leds[NUM_LEDS];
uint8_t color[3] = {255, 134, 26};

void setColor(unsigned char *clr) {
  int i;
  for (i = 0; i < NUM_LEDS; i++) {
    leds[i].r = clr[0];
    leds[i].g = clr[1];
    leds[i].b = clr[2]; 
  }
  FastLED.show();
}

void Blink(Byte n) {
  int i;
  for (i = 0; i < n; i++) {
    digitalWrite(LED, HIGH);
    delay(10);
    digitalWrite(LED, LOW);
    delay(10);
  } 
}

void colorHandler(unsigned char cmd, unsigned int len, void *data) {
   setColor((unsigned char *)data);
}

void setup() {
  // Initialize MCom w/ RF communicator
  MCom_init(64, 1, 1, 100, Blink);
  MCom_Arduino_rf433_init(RF433, 0, DATA, 1000);
  MCom_bind(CMD_COLOR, 3, colorHandler);
  // Initialize leds
  FastLED.addLeds<NEOPIXEL, COLOR, GRB>(leds, NUM_LEDS);
  pinMode(LED, OUTPUT);
  
  setColor(color);
}

void loop() {
  if (MCom_get(RF433)) {
    Blink(1);
  }
}
