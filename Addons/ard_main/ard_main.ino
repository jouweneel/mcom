#include <VirtualWire.h>
#include <FastLED.h>
#include <LiquidCrystal.h>
#include <LCDisplay.h>
#include <LEDStrip.h>

#include <ard_main_shared.h>

#include <MCom_defines.h>
#include <MCom.h>
#include <MCom_Arduino_rs232.h>
#include <MCom_default_commands.h>

#include <ard_main_pinout.h>

#define NR_LEDS 50
#define COM     0

LCDisplay *lcd;
LEDStrip *screen;

// LED command handlers
void ledPower(Byte cmd, Uint size, void *data)      { screen->power(*((Byte *)data)); }
void ledBrightness(Byte cmd, Uint size, void *data) { screen->brightness(*((Byte *)data)); }
void ledColor(Byte cmd, Uint size, void *data)      { screen->color((CRGB *)data); }
void ledColors(Byte cmd, Uint size, void *data)     { screen->colors((CRGB *)data); }

// LCD command handlers
void lcdPower(Byte cmd, Uint size, void *data)      { lcd->power(*((Byte *)data)); }
void lcdBrightness(Byte cmd, Uint size, void *data) { lcd->brightness(*((Byte *)data)); }
void lcdClear(Byte cmd, Uint size, void *data)      { lcd->clear(); }
void lcdPrint(Byte cmd, Uint size, void *data)      { lcd->print((char *)data); }
void lcdPrint0(Byte cmd, Uint size, void *data)     { lcd->print0((char *)data); }
void lcdPrint1(Byte cmd, Uint size, void *data)     { lcd->print1((char *)data); }
void lcdProgress(Byte cmd, Uint size, void *data)   { lcd->progress(*((Byte *)data)); }

void setup() {
  // Set non-library pins
  pinMode(LED, OUTPUT); digitalWrite(LED, LOW);
  pinMode(LCD_VDD, OUTPUT); digitalWrite(LCD_VDD, HIGH);

  // Initialize devices
  Serial.begin(115200);
  lcd = new LCDisplay(LCD_MODE, LCD_SET, LCD_D0, LCD_D1, LCD_D2, LCD_D3, LCD_BRGHT);
  screen = new LEDStrip(NR_LEDS);
  FastLED.addLeds<NEOPIXEL, LED_PIN, GRB>(screen->getLeds(), NR_LEDS);
  screen->setColor();

  // Initialize MCom
  MCom_init(256, 2, 11, 1000000);
  MCom_Arduino_rs232_init(COM, Serial);

  // Bind LED commands
  MCom_bind(LED_POWER,      1,          ledPower);
  MCom_bind(LED_BRIGHTNESS, 1,          ledBrightness);
  MCom_bind(LED_COLOR,      3,          ledColor);
  MCom_bind(LED_COLORS,     3*NR_LEDS,  ledColors);

  // Bind LCD commands
  MCom_bind(LCD_POWER,      1,          lcdPower);
  MCom_bind(LCD_BRIGHTNESS, 1,          lcdBrightness);
  MCom_bind(LCD_CLEAR,      0,          lcdClear);
  MCom_bind(LCD_PRINT,      33,         lcdPrint);
  MCom_bind(LCD_PRINT0,     17,         lcdPrint0);
  MCom_bind(LCD_PRINT1,     17,         lcdPrint1);
  MCom_bind(LCD_PROGRESS,   1,          lcdProgress);
}

void loop() {
  MCom_get(COM);
}

