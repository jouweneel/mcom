var run = require('child_process').spawn,
    test = run('ScreenLight/Release/ScreenLight.exe', ['3', '18', '10', '6', '0']);

test.stdout.on('data', function (data) {
    var lights = [];
    for (var i = 0; i < data.length; i++) {
        lights[i] = data.readUInt8(i);
    }
    console.log(lights);
});

setTimeout(function () {
    console.log('Quitting');
    test.kill();
}, 2000);
