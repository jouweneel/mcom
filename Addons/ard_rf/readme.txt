ard_raspi
---------
Programs an Arduino to read commands from RS232, and relay these via an RF433 radio transmitter to two different receivers.

Things to configure:
- enum {CMD_LIGHTCOLOR, CMD_SCREENCOLOR} -> Sets MCom commands. May also be set with #define CMD_LIGHTCOLOR 0x..
- #define DATA -> RF433 data pin
- #define VCC/GND -> Only use if, like me, you power the RF433 chip via 2 gpio pins (which isn't good practice, but hey, it works)
