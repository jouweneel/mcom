#include <VirtualWire.h>
#include <FastLED.h>

#include <MCom_defines.h>
#include <MCom.h>
#include <MCom_Arduino_rs232.h>
#include <MCom_Arduino_rf433.h>
#include <MCom_default_commands.h>

enum {COM, RF};

#define DATA 4
#define LED  13

CRGB leds, light, off;
float brightness;

void setColor(CRGB clr) {
  leds.r = clr.r * brightness;
  leds.g = clr.g * brightness;
  leds.b = clr.b * brightness;
  MCom_send(RF, LIGHT_COLOR, &leds);
}

// MCom Callbacks
void heartBeat(Byte cmd, Uint len, void *data){
  MCom_send(RF, MCOM_HEARTBEAT, 0);
}
void lightColor(Byte cmd, Uint len, void *data){
  CRGB clr = *((CRGB *)data);
  light.r = clr.r; light.g = clr.g; light.b = clr.b;
  setColor(light);
}
void lightBrightness(Byte cmd, Uint len, void *data){
  Byte byteBrightness = *((Byte *)data);
  brightness = ((float)byteBrightness)/255.0;
  setColor(light);
}
void lightPower(Byte cmd, Uint len, void *data){
  Byte on = *((Byte *)data);
  if (on) setColor(light);
  else setColor(off);
}

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(115200);

  MCom_init(64, 2, 4, 250);
  MCom_Arduino_rs232_init(COM, Serial);
  MCom_Arduino_rf433_init(RF, DATA, 0, 1000);

  MCom_bind(MCOM_HEARTBEAT,   0, heartBeat);
  MCom_bind(LIGHT_COLOR,      3, lightColor);
  MCom_bind(LIGHT_BRIGHTNESS, 1, lightBrightness);
  MCom_bind(LIGHT_POWER,      1, lightPower);

  off.r = 0; off.g = 0; off.b = 0;
  light.r = 255; light.g = 134; light.b = 26;
  brightness = 1;
  lightColor(0, 0, &light);
}

void loop() {
   MCom_get(COM);
   delay(1);
}
