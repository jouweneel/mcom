#ifndef _RS232_H_
#define _RS232_H_

#include <Windows.h>
#include <stdlib.h>
#include <string>

#include "../MCom_defines.h"
#include "../MCom.h"

bool MCom_rs232_init(unsigned char _id, unsigned int _comPort, unsigned int _baudRate);
unsigned int MCom_rs232_transmit(Buffer *_buffer, int comParam);
unsigned int MCom_rs232_receive(Buffer *_buffer, int comParam);

#endif
