#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "tcpclient.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

WSADATA wsaData;
struct addrinfo *result = NULL, *ptr = NULL, hints;
int iResult;

bool MCom_tcpclient_init(unsigned char _id, PCSTR hostname, PCSTR port) {
    int socketFd = INVALID_SOCKET;
    // Start WSA
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error %d\n", iResult);
        return false;
    }
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve server
    iResult = getaddrinfo(hostname, port, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed with error %d\n", iResult);
        WSACleanup();
        return false;
    }

    // Attempt to connect
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        socketFd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (socketFd == INVALID_SOCKET) {
            printf("socket failed with error %d\n", WSAGetLastError());
            WSACleanup();
            return false;
        }

        iResult = connect(socketFd, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(socketFd);
            socketFd = INVALID_SOCKET;
            continue;
        }
        break;
    }
    freeaddrinfo(result);

    if (socketFd == INVALID_SOCKET) {
        printf("Unable to connect to server\n");
        WSACleanup();
        return false;
    }

    bindCom(_id, MCom_tcpclient_transmit, MCom_tcpclient_receive, socketFd);
    printf("Connected\n");
    return true;
}
Uint MCom_tcpclient_transmit(Buffer *buffer, int comParam) {
    //printf("Sending cmd %x\n", buffer->buf[0]);
    iResult = send(comParam, (char *)buffer->buf, buffer->ptr, 0);
    return iResult;
}
Uint MCom_tcpclient_receive(Buffer *buffer, int comParam) {
    iResult = recv(comParam, (char *)buffer->buf, buffer->size, 0);
    if (iResult <= 0) {
        printf("Connection closed or error");
        iResult = 0;
    }
    return iResult;
}
