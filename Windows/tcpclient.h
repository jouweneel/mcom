#ifndef _TCPCLIENT_H_
#define _TCPCLIENT_H_

#define _WINSOCKAPI_
#define WIN32_LEAN_AND_MEAN

#include "../MCom_defines.h"
#include "../MCom.h"

#include <windows.h>

bool MCom_tcpclient_init(unsigned char _id, PCSTR hostname, PCSTR port);
Uint MCom_tcpclient_transmit(Buffer *_buffer, int comParam);
Uint MCom_tcpclient_receive(Buffer *_buffer, int comParam);

void MCom_tcpclient_start();

#endif
