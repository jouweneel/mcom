#ifndef _STDIO_H_
#define _STDIO_H_

#include <stdio.h>

#include "../MCom_defines.h"
#include "../MCom.h"

Byte MCom_stdio_init(Byte id);
Uint MCom_stdio_transmit(Buffer *buf);
Uint MCom_stdio_receive(Buffer *buf);

#endif
