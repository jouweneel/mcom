#include <thread>
#include "rs232.h"

using namespace std;

COMSTAT status;
DCB params;
COMMTIMEOUTS timeouts;
DWORD error;

bool MCom_rs232_init(unsigned char _id, unsigned int _comPort, unsigned int _baudRate) {
    wstring comName = L"\\\\.\\COM";
    comName.append(to_wstring(_comPort));

    HANDLE comHandle = CreateFile(
        comName.c_str(),
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
        );
    if (comHandle == INVALID_HANDLE_VALUE) {
        printf("Connect Error: invalid handle\n");
        return false;
    }
    else {
        GetCommState(comHandle, &params);
        params.BaudRate = _baudRate;
        params.ByteSize = 8;
        params.StopBits = ONESTOPBIT;
        params.Parity = NOPARITY;
        if (!SetCommState(comHandle, &params)) {
            printf("Connect Error: Couldn't set CommState\n");
            return false;
        }

        GetCommTimeouts(comHandle, &timeouts);
        timeouts.ReadIntervalTimeout = 1;
        timeouts.ReadTotalTimeoutConstant = 1;
        timeouts.ReadTotalTimeoutMultiplier = 1;
        timeouts.WriteTotalTimeoutConstant = 1;
        timeouts.WriteTotalTimeoutMultiplier = 1;
        if (!SetCommTimeouts(comHandle, &timeouts)) {
            printf("Connect Error: Couldn't set timeouts\n");
            return false;
        }
    }

	// Bind communicator to mCom library
	bindCom(_id, MCom_rs232_transmit, MCom_rs232_receive, (int)comHandle);

    return true;
}

unsigned int MCom_rs232_transmit(Buffer *_buffer, int comParam) {
    DWORD nrBytes = 0;
    ClearCommError((HANDLE)comParam, &error, &status);

    if (WriteFile((HANDLE)comParam, _buffer->buf, _buffer->ptr, &nrBytes, NULL)) {
        return (unsigned int)nrBytes;
    }
    else {
        ClearCommError((HANDLE)comParam, &error, &status);
        printf("Transmit Error\n");
    }

    return 0;
}

unsigned int MCom_rs232_receive(Buffer *_buffer, int comParam) {
    DWORD nrBytes = 0;
    ClearCommError((HANDLE)comParam, &error, &status);
    if (ReadFile((HANDLE)comParam, _buffer->buf, status.cbInQue, &nrBytes, NULL)) {
        return (unsigned int)nrBytes;
    }

    return 0;
}


