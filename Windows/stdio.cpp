#include "stdio.h"

Byte MCom_stdio_init(Byte id) {
    bindCom(id, MCom_stdio_transmit, MCom_stdio_receive);
    return 1;
}

Uint MCom_stdio_transmit(Buffer *buf) {
    Uint nrBytes = fwrite(buf->buf, 1, buf->ptr, stdout);
    fflush(stdout);
    return nrBytes;
}

Uint MCom_stdio_receive(Buffer *buf) {
    Uint nrBytes = 1; //scanf("%s", buf->buf); //fread(buf->buf, 1, buf->size, stdin);
    printf("Read %d bytes\n", nrBytes);
    return nrBytes;
}
