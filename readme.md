MCom - Simple eMbedded Communication Protocol
=============================================

MCom aims to be a cross-platform, bytewise protocol to communicate between embedded platforms and hosting systems. To this end, it is kept as small and simple as possible. This means there is only one shared buffer for both transmitting and receiving - which also means these functions are blocking.

Supported platforms
================
Currently, the source has been tested on Windows (c/c++), Arduino and Raspberry Pi (node.js, check https://www.npmjs.com/package/MCom). The protocol itself is platform independent; the idea is that more communicators will be added for other platforms.

These communicators have to implement 3 functions:

- Initialization (which registers the communicator in the main library and any necessary initializations)
- Transmit (which can transmit a unsigned char array of a given length)
- Receive (which can read/receive to a unsigned char array, returning the nr of read bytes)

How to use
==========
Add to your project
------------------------

- Add and include MCom_defines.h and MCom.c/h to your source (for Arduino, add a folder 'MCom' to your Libraries folder, located in 'My Documents/Arduino/')
- Add and include the communicators you want to use (e.g. RS232) from the right platform (Windows, Arduino)

Initialize / use
-----------------

1. Enum your communicators, with names you think are handy (i.e. enum {RS232, RF433 }
2. \#define your commands with an arbitrary command code (i.e. #define CMD_COLOR 0x01), ranging from 0x00 to 0xFD (or 0 to 253)
3. Call MCom_init(uint bufsize, uchar nrCommunicators, uchar nrCommands, uint getTimeout), which defines a fixed (shared) buffer size, the number of communicators you want to add (i.e. the size of your enum), the number defined commands and the get() timeout ( = the number of gets after which an incomplete packet isn't expected to be arriving anymore).
4. Initialize the communicators by calling MCom_[platform]_[communicator]_init(id, ...). As ID, use the enum from (1). Different platforms require different additional arguments; check the header file.
5. Bind sizes and callback functions to your commands, by calling MCom_bind(Command, nrDataBytes, function(uchar cmd, uint size, void *data)) - that is, the #defined command, the nr of bytes of data payload, and finally a function name which you defined above. Now, when a packet arrives, the bound function will automatically be called. 

In case you only want to transmit (i.e. don't need a callback), don't provide a function, but rather  the number 0.

Add custom communicator
-----------------------

If you need a communicator that isn't implemented yet, you can find a template with discription in the Template folder. If it's working, please make a Pull request on git, so others can use it as well! Don't forget to add a folder for the used platform, if necessary.