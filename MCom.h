/*
 * MCom.h
 *
 * Function prototypes for MCom
 */

#ifndef _MCOM_H_
#define _MCOM_H_

#include "MCom_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

	/* Public library functions */
	Byte MCom_init( int _bufSize, Byte _nrComs,  Byte _nrBindings, Uint _getTimeout);
	void MCom_clean();

    Byte MCom_bind(Byte cmd, Uint size, 
        void(*handler)(Byte cmd, Uint size, void *data)
    );

	Uint MCom_send(Byte id, Byte cmd, void *data);
	Uint MCom_get(Byte id);

	/* 'Internal' library functions (used by Connectors) */
	Byte bindCom(Byte id, 
        Uint(*transmit)(Buffer *buffer, int comParam), 
        Uint(*receive)(Buffer *buffer, int comParam),
        int comParam
    );

#ifdef __cplusplus
}
#endif	// extern "C"

#endif	// _MCOM_H_
