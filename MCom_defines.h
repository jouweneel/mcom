/*
 * MCom Defines
 *
 * Type, byte and struct definitions
 */

#ifndef _MCOM_DEFINES_H_
#define _MCOM_DEFINES_H_

#ifdef __cplusplus
extern "C" {
#endif

// Shorthand for unsigned char
#define Byte unsigned char
#define Uint unsigned int

// End byte
#define END 0xFE

// Buffer struct
typedef struct Buffer {
	Uint ptr;       // Pointer to next free byte (= current data size)
	Byte *buf;      // Actual buffer holding the packets
	Uint size;		// Buffer size (in Bytes)
} Buffer;

// Handler Binding struct
typedef struct HandlerBinding {
	Byte cmd;                                           // Command to handle
	Uint size;                                          // # of data bytes
	void(*function)(Byte cmd, Uint size, void *data);   // Handler function pointer
} HandlerBinding;

// Communicator Binding struct (holds ID, transmit() and receive() functions
typedef struct ComBinding {
	Uint(*transmit)(Buffer *buffer, int comParam);      // Transmit function for communicator
	Uint(*receive)(Buffer *buffer, int comParam);       // Receive function for communicator
    int comParam;                                       // Com parameter (i.e. file descriptor)
} ComBinding;

#ifdef __cplusplus
}
#endif	// extern "C"

#endif	// _MCOM_DEFINES_H_
