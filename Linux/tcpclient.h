#ifndef _TCPCLIENT_H_
#define _TCPCLIENT_H_

#include "../MCom_defines.h"
#include "../MCom.h"

bool MCom_tcpclient_init(Uint id, char *hostname, int port);
Uint MCom_tcpclient_transmit(Buffer *buffer, int comParam);
Uint MCom_tcpclient_receive(Buffer *buffer, int comParam);

#endif
