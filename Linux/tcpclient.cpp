#include "tcpclient.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

int result;
struct sockaddr_in serveraddr;
struct hostent *server;

bool MCom_tcpclient_init(Uint id, char *hostname, int port) {
    // Create socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("socket creatin failed with error %s\n", sockfd);
        return false;
    }

    // Resolve server
    server = gethostbyname(hostname);
    if (server == NULL) {
        printf("gethostbyname failed for host %s\n", hostname);
        return false;
    }

    // Build server internet address
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(port);

    // Connect to server
    result = connect(sockfd, (sockaddr *)&serveraddr, sizeof(serveraddr));
    if (result < 0) {
        printf("connect failed with error %d\n", result);
        return false;
    }

    // Bind communicator in MCom
    // @comParam: socket file descriptor
    bindCom(id, MCom_tcpclient_transmit, MCom_tcpclient_receive, sockfd);
    printf("Connected to %s:%d\n", hostname, port);
    return true;
}

Uint MCom_tcpclient_transmit(Buffer *buffer, int comParam) {
    int nrBytes = write(comParam, buffer->buf, buffer->ptr);
    if (nrBytes < 0) {
        printf("tcpclient_transmit failed with error %d\n", nrBytes);
        nrBytes = 0;
    }
    return nrBytes;
}

Uint MCom_tcpclient_receive(Buffer *buffer, int comParam) {
    int nrBytes = read(comParam, buffer->buf, buffer->size);
    if (nrBytes < 0) {
        printf("tcpclient_receive failed with error %d\n", nrBytes);
        nrBytes = 0;
    }
    return nrBytes;
}
