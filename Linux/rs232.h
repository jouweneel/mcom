#ifndef _RS232_H_
#define _RS232_H_

#include <stdlib.h>
#include <string.h>

#include "../MCom_defines.h"
#include "../MCom.h"

bool MCom_rs232_init(Byte id, const char *ttyName, Uint baudRate);
unsigned int MCom_rs232_transmit(Buffer *buffer, int comParam);
unsigned int MCom_rs232_receive(Buffer *buffer, int comParam);

#endif
