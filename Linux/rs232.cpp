#include "rs232.h"

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

bool MCom_rs232_init(Byte id, const char *ttyName, Uint baudRate) {
    // Open specified port
    char portname[64];
    sprintf(portname, "/dev/tty%s", ttyName);
    int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening file descriptor to %s\n", portname);
        return false;
    }

    // Set attributes
    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0) {
        printf("Error getting attributes from %s\n", portname);
        return false;
    }
    cfsetospeed(&tty, baudRate);
    cfsetispeed(&tty, baudRate);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    tty.c_iflag &= ~IGNBRK;
    tty.c_lflag = 0;
    tty.c_oflag = 0;
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 5;
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_cflag |= (CLOCAL | CREAD);
    tty.c_cflag &= ~(PARENB | PARODD);
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error setting attributes to %s\n", portname);
        return false;
    }

    // Bind communicator in MCom 
    // @comParam: file descriptor
    bindCom(id, MCom_rs232_transmit, MCom_rs232_receive, fd);
    
    sleep(2);

    printf("Connected to %s\n", portname);
    return true;
}
unsigned int MCom_rs232_transmit(Buffer *buffer, int comParam) {
    write(comParam, buffer->buf, buffer->ptr);
    usleep(120 * buffer->ptr);
    return buffer->ptr;
}
unsigned int MCom_rs232_receive(Buffer *buffer, int comParam) {
    int n = read(comParam, buffer->buf, buffer->size);
    buffer->ptr = n;
    return n;
}

