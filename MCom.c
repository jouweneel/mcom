/*
* MCom.c
*
* Main shared MCom functions
*/

#ifdef __linux__
#include <unistd.h>
#endif
#ifdef _WIN32
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include "MCom.h"


/* Private variables */
Buffer buffer;              // Send/receive buffer

Byte nrComs;                // Total # of communicators
ComBinding *coms;           // Array with communicators

Byte nrHandlers;            // Total # of handlers
HandlerBinding *handlers;   // Array with command handlers
Byte handlerCount;          // Current index of handler

Uint getTimeout;            // Message get timeout (in # of loops)
Uint getTimeoutCount;       // Current timeout value

/* Encodes a message into the buffer
* @_cmd Command code
* @_data Payload data
*
* @return Nr of encoded bytes OR 0 if invalid
*/
Uint Encode(Byte _cmd, void *_data) {
    // Find bound command
    int i;
    for (i = 0; i < nrHandlers; i++) {
        if (_cmd == handlers[i].cmd) {
            // Valid command, so encode packet
            buffer.buf[0] = _cmd;
            Byte *data = (Byte *)_data;
            for (buffer.ptr = 1; buffer.ptr <= handlers[i].size; buffer.ptr++) {
                buffer.buf[buffer.ptr] = data[buffer.ptr - 1];
            }
            buffer.buf[buffer.ptr] = END;
            buffer.ptr++;
            return buffer.ptr;
        }
    }

    return 0;
}

/* Decodes incoming data
* @return Handlers[] index (= Command), 0xFE (No or Flushed data), 0xFF (Incomplete packet)
*/
unsigned char Decode() {
    unsigned char cmd;

    if (buffer.ptr > 0) {										// Check if data is present
        cmd = buffer.buf[0];
        int i;
        for (i = 0; i < nrHandlers; i++) {						// Find corresponding handler index
            if (cmd == handlers[i].cmd) {
                if (buffer.ptr >= (handlers[i].size + 2)) {		// Check if enough data is present
                    if (buffer.buf[buffer.ptr - 1] == END) {	// Check for valid packet
                        return i;
                    }
                }
                else {											// Incomplete data present
                    return 0xFF;
                }
            }
        }
    }
    // Reached if no data present or command wasn't bound
    buffer.ptr = 0;
    return 0xFE;
}

/* Bind a Communicator to an ID
* @id ID of communicator (0 < id < nrComs)
* @_transmit Uint(*function)(Buffer)
* @_receive Uint(*function)(Buffer)
*
* @return 1 if bound, 0 otherwise
*/
Byte bindCom(Byte id, 
    Uint(*transmit)(Buffer *buffer, int _comParam), 
    Uint(*receive)(Buffer *buffer, int _comParam), int comParam) {
    if (id < nrComs) {
        coms[id].transmit = transmit;
        coms[id].receive = receive;
        coms[id].comParam = comParam;
        return 1;
    }

    return 0;
}

/* Initialize shared buffer, Communicator array and Bindings array
* @_bufSize	Shared buffer size (in bytes)
* @_nrComs		Number of Communicators
* @_nrHandlers	Number of Handlers (/bound commands)
* @_getTimeout	Number of MCom_get iterations after which incomplete packet is flushed
*
* @return 1 if OK, 0 otherwise
*/
Byte MCom_init(int bufSize, Byte _nrComs, Byte _nrHandlers, Uint _getTimeout) {
    buffer.ptr = 0;
    buffer.buf = (Byte *)malloc(bufSize*sizeof(Byte));
    buffer.size = bufSize;

    nrComs = _nrComs;
    coms = (ComBinding *)malloc(nrComs*sizeof(ComBinding));

    nrHandlers = _nrHandlers;
    handlers = (HandlerBinding *)malloc(nrHandlers*sizeof(HandlerBinding));
    handlerCount = 0;

    getTimeout = _getTimeout;
    getTimeoutCount = _getTimeout;

    if (!buffer.buf || !handlers || !coms) {
        return 0;
    }
    return 1;
}

/* Free arrays */
void MCom_clean() {
    free(buffer.buf);
    free(handlers);
    free(coms);

    return;
}

/* Bind a Command to a data size and, if provided, a handler function
* @_cmd	  Command to be bound (0 < cmd < 0xFE)
* @_size	  Nr of data bytes
* @_function Callback function(Byte, Uint, void *) pointer
*
* @return 1 of bound, 0 otherwise
*/
Byte MCom_bind(Byte cmd, Uint size, void(*handler)(Byte cmd, Uint size, void *data)) {
    if ((handlerCount == nrHandlers) || (size > (buffer.size - 2))) {
        // Sanity check
        return 0;
    }
    handlers[handlerCount].cmd = cmd;
    handlers[handlerCount].size = size;
    handlers[handlerCount].function = handler;
    handlerCount++;

    return 1;
}

/* Send a Command with Data over the specified Communicator id
* @id Communicator ID
* @cmd Command to be sent
* @data Pointer to data to be sent
*
* @return Nr of sent bytes OR 0
*/
Uint MCom_send(Byte id, Byte cmd, void *data) {
    Uint encoded = Encode(cmd, data);
    if (encoded) {
        Uint sent = coms[id].transmit(&buffer, coms[id].comParam);
        for (int i = 0; i < 6; i++) {
            printf("%x ", buffer.buf[i]);
        }
        if (sent) {
            buffer.ptr = 0;
            return sent;
        }
    }
    buffer.ptr = 0;
    return 0;
}

/* Get data from the specified Communicator id
* @id Communicator ID
*
* @return 1 if a command has been processed, 0 otherwise
*/
Uint MCom_get(Byte id) {
    Uint nrBytes = coms[id].receive(&buffer, coms[id].comParam);
    Byte index = 0xFE;							// Init with empty buffer code

    buffer.ptr += nrBytes;
    index = Decode();

    if (index < 0xFE) {
        Byte cmd = buffer.buf[0];
        void *data = (void *)&buffer.buf[1];
        handlers[index].function(cmd, handlers[index].size, data);	// Call bound function
        buffer.ptr = 0;						// Flush buffer
        getTimeoutCount = getTimeout;		// Reset timeout counter
        return 1;
    }

    if (index == 0xFF) {					// Incomplete packet read
        getTimeoutCount--;
        if (getTimeoutCount <= 0) {			// Timeout has been reached
            buffer.ptr = 0;					// Flush buffer
            return 0;						// Exit
        }
        else {
#ifdef __linux__
            usleep(50);
#endif
#ifdef _WIN32
            Sleep(1);
#endif
            MCom_get(id);					// Keep checking until timeout
        }
    }

    return 0;
}
