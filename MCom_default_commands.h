#ifndef MCOM_DEFAULT_COMMANDS_H_
#define MCOM_DEFAULT_COMMANDS_H_

// MCom general commands            // Per-device bytes
#define MCOM_HEARTBEAT      0x00    // 0
#define MCOM_BROADCAST      0x01    // 2 + device-specific

// MCom light commands
#define LED_POWER           0x10    // 1 (Off/On)
#define LED_BRIGHTNESS      0x11    // 1 (Intensity)
#define LED_COLOR           0x12    // 3 (RGB)
#define LED_COLORS          0x13    // 3*#leds (RGB)

// MCom text commands (16x2 display specific)
#define LCD_POWER           0x20
#define LCD_BRIGHTNESS      0x21
#define LCD_CLEAR           0x22    // 0
#define LCD_PRINT           0x23    // 33 (2*16 + 1)
#define LCD_PRINT0          0x24    // 17 (16 + 1)
#define LCD_PRINT1          0x25    // 17 (16 + 1)
#define LCD_PROGRESS        0x26    // 1 (progress, 0-15)

// MCom Interface commands
#define IF_POWER            0xe0;
#define IF_BRIGHTNESS       0xe1;
#define IF_COLOR            0xe2;

#endif  // MCOM_DEFAULT_COMMANDS_H_
