#ifndef _MCOM_ARDUINO_RS232_H_
#define _MCOM_ARDUINO_RS232_H_

#include <HardwareSerial.h>
#include "MCom_defines.h"
#include "MCom.h"

Byte MCom_Arduino_rs232_init(Byte id, HardwareSerial &_serial);
Uint MCom_Arduino_rs232_transmit(Buffer *buffer, int comParam);
Uint MCom_Arduino_rs232_receive(Buffer *buffer, int comParam);

#endif
