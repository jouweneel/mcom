#include "MCom_Arduino_rf433.h"

unsigned char MCom_Arduino_rf433_init(unsigned char id, unsigned char txPin, unsigned char rxPin, unsigned int rate) {
	if (txPin > 0) {
		vw_set_tx_pin(txPin);
		vw_set_ptt_inverted(true);
		vw_setup(rate);
	}

	if (rxPin > 0) {
		vw_set_rx_pin(rxPin);
		vw_set_ptt_inverted(true);
		vw_setup(rate);
		vw_rx_start();
	}

	// Bind communicator in mCom library
	return bindCom(id, MCom_Arduino_rf433_transmit, MCom_Arduino_rf433_receive, 0);
}

unsigned int MCom_Arduino_rf433_transmit(Buffer *buffer, int comParam) {
	if (!vw_send(buffer->buf, buffer->ptr)) {
		return 0;
	}
	vw_wait_tx();

	return buffer->ptr;
}

unsigned int MCom_Arduino_rf433_receive(Buffer *buffer, int comParam) {
	Byte nrBytes = buffer->size - buffer->ptr;
	vw_get_message(&buffer->buf[buffer->ptr], &nrBytes);
	return nrBytes;
}
