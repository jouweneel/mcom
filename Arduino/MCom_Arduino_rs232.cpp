#include "MCom_Arduino_rs232.h"

HardwareSerial *serial;

Byte MCom_Arduino_rs232_init(Byte id, HardwareSerial &_serial) {
	serial = &_serial;
	// Bind communicator in mCom library
	return bindCom(id, MCom_Arduino_rs232_transmit, MCom_Arduino_rs232_receive, 0);
}

Uint MCom_Arduino_rs232_transmit(Buffer *buffer, int comParam) {
	Uint nrBytes = serial->write(buffer->buf, buffer->ptr);
	return nrBytes;
}

Uint MCom_Arduino_rs232_receive(Buffer *buffer, int comParam) {
	Uint nrBytes = serial->available();
	if (nrBytes > (buffer->size - buffer->ptr)) {
		nrBytes = buffer->size - buffer->ptr;
	}

	if (nrBytes) {
		serial->readBytes((char *)&buffer->buf[buffer->ptr], nrBytes);
		return nrBytes;
	}
	return 0;
}
