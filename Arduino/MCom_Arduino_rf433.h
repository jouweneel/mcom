#ifndef _MCOM_ARDUINO_RF433_H_
#define _MCOM_ARDUINO_RF433_H_

#include <VirtualWire.h>

#include "MCom_defines.h"
#include "MCom.h"

unsigned char MCom_Arduino_rf433_init(unsigned char id, unsigned char txPin, unsigned char rxPin, unsigned int rate);
unsigned int MCom_Arduino_rf433_transmit(Buffer *_buffer, int comParam);
unsigned int MCom_Arduino_rf433_receive(Buffer *_buffer, int comParam);

#endif
